# GTbuild 
_Doing More with Less_

## Main features
- allows specifying dependencies between scripts including transitive ones
- automatically generates a patch based on a Git diff
- a single shell script of 150 lines
- the runtime is 1/10 of a second

## How to install

Download [gtbuild.sh](gtbuild.sh) 

## How to study

- Download [gtbuild_test.sh](gtbuild_test.sh) at the same place where [gtbuild.sh](gtbuild.sh) is downloaded
- Read the script and the [Example](#example) described below
- Run [gtbuild_test.sh](gtbuild_test.sh) 
- It will create a temporary directory somesing like /tmp/2016-10-27 containing a zip file and auxilary files used to build it

## How to use

Your project has to be under Git and has to have at least one annotated tag

You need to create a buildfile in your project directory
then run

``` shell
$> gtbuild.sh
```

## Example

We have 
* a bunch of files organazed as shown in in [Directory structure] (#directory-structure)
* a [Buildfile] (#buildfile) 

We run [gtbuild.sh](gtbuild.sh) and it produces two files:
* [a patch file] (#patch-file)
* [a "full" file] (#full-file)

### Directory structure
* comp_a/
   * a.tab _(depends on b.ck)_
   * a.trg _(changed)_
   * a.idx _(changed)_
   * b.ck  
   * b.tab _(depends on c.vw)_
   * c.vw
* parameters.sql
* patches/
   * cr_001.sql
   * cr_002.sql _(changed)_

Note that 
* the files a.trg, a.idx, cr_002.sql have been changed since the last release
* the file a.tab depends on the file b.ck and b.tab on c.vw. Thus a.tab transitively depends on c.vw because a.tab -> b.ck -> b.tab -> c.vw
* the files are listed in an alphabetical order

### Buildfile

###### Parameters

| Parameter | Value| Description |
| ---- | ----- | ----- |
| build order | tab, idx, ck, vw, trg, sql | Ordered (left to right) list of file extensions. First elements are build first |
| statless files | trg, vw | List of file extensions not having any state |

###### Filters

| Filter | Flag | Description of the flag |
| ---- | :---: | ----- |
| parameters.sql | * | a file will be included regardless of whether it has  been changed |
| comp_a | . | the extension of a file is to be in the list of "Statless files"  when creating a patch file. In the full mode the filter is applied to all files |
| patches | ! | the changed file will be included in the patch and skipped (changed or unchanged) during the creation of the full-mode-file |

### Patch file

###### Included files

| File | Inclusion description  | Ordering description |
| ---------------- | ----------- | ------- |
| parameters.sql  | it is unchanged but the flag "*" overides it  | It is in the first palce of the Buildfile |
| comp_a/a.trg | it is changed and it is in the list of  "Statless files" | It is in the second palce of the Buildfile |
| patches/cr_002.sql | it is changed and the flag "!" | It is in the third palce of the Buildfile |

###### Excluded files

| File | Description |
| ---------------- | ----------- |
| a.tab, b.tab, b.ck, c.vw and, cr_001.sql | have not been changed |
| a.idx | is not in the list of "Statless files" even though it has been changed |

### Full-file

###### Included files

| File | Inclusion description | Ordering description |
| ---------------- | :-----------: | ----------- |
| parameters.sql  | flag "*" |  It is in the first palce of the Buildfile |
| comp_a/c.vw | flag "." | b.tab explicitly depends on c.vw |
| comp_a/b.tab | flag "." | explicitly depends on c.vw |
| comp_a/b.ck | flag "." | According to the "build order" defined in Buildfile |
| comp_a/a.tab | flag "." | explicitly depends on b.ck |
| comp_a/a.idx | flag "." | According to the "build order" |
| comp_a/a.trg | flag "."  | According to the "build order" |

###### Excluded files

| File | Exclusion description |
| ---------------- | ----------- |
| cr_001.sql, cr_002.sql | flag "!" |
