#!/usr/bin/env bash

# Copyright 2016 Sergey Rudyshin. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This is a build tool for scripts

set -e

BASETAG="*"
TARGETS="main"
BUILDFILE="./build.gts"

function usage () {
    cat <<EOF
Usage: $0 [options] [<buildfile>]

Command line options:
    -b|--basetag    Pattern for the baseline's tag. Default is "*"
    -t|--targets    List of the targets to be build. Default is "main"

Default value for <buildfile> is "./build.gts"

Buildfile contains:
    - filters to select files for the target script
    - options

Buildfile options:
    PAR_BUILD_ORDER             Ordered (left to right) list of file extensions. First elements are build first
    PAR_STATELESS_EXTS          List of file extensions not having any state
    PAR_INSTALL_SCRIPT_EXT      Extension for the output script
    PAR_ROOT_PATH               Path from <buildfile> to the root of the module
    PAR_DEPENDS_REGEXP          Expression determining the string which describes dependencies
    PAR_INC_PREF                Prefix to be inserted after expansion the keyword @inc
    PAR_VERSION_FILE            Name of the file containing the current version (commit)

Buildfile filter format:
    A filter consists of three columns separated by spaces:
    col#1  - a substring of the file name
    col#2  - flag which can contain one of the following 
             *) meaning that the file will be examined regardless of whether it has been changed;
             !) the file will be included in the patch and skipped during the creation of the full-mode-file;
             .) the extension of the file is to be in PAR_STATELESS_EXTS.
    col#3  - a value should match with the command line option "targets" 
             otherwise the rule is not used to build the target
EOF
    exit 1
}

while [[ $# -gt 1 ]]
do
    case "$1" in
        -b|--basetag)        
        BASETAG="$2"; shift ;;
        -t|--targets)
        TARGETS="$2"; shift ;;
        *)
        echo \"$1\" is an invalid option
        echo
        usage "$1"
        ;;
    esac
    shift
done

[[ -n "$1" ]] && BUILDFILE="$1"

PAR_BUILD_ORDER="sql tab seq dat idx sta ck ref vw pks pkb fnc prc grt trg"
PAR_STATELESS_EXTS="vw pks pkb fnc prc trg sta grt"
PAR_INSTALL_SCRIPT_EXT=".sql"
PAR_ROOT_PATH="."
PAR_DEPENDS_REGEXP=".*@depends on: *"
PAR_INC_PREF="@"
PAR_VERSION_FILE="version"

PARAMS=$(grep 'PAR_.*=' -- "$BUILDFILE") 
eval "$PARAMS"

for opt in PAR_BUILD_ORDER PAR_STATELESS_EXTS PAR_INSTALL_SCRIPT_EXT PAR_ROOT_PATH PAR_DEPENDS_REGEXP PAR_VERSION_FILE
do
    [[ "$(eval "echo \"\$$opt\"")" ]] || (echo "Please specify $opt"; echo ; usage)
done

set -u

mkdir -p "${TMP_DIR:=/tmp/$(date '+%F_%H%M%S').$$/$$}"

cd "${ROOT_PATH:=${BUILDFILE%/*}/$PAR_ROOT_PATH}"

BASELINE_TAG=$(git describe --match "${BASETAG}" --abbrev=0)
ARCHIVE=${TMP_DIR%/$$}/$(git describe --match "${BASETAG}" --dirty).zip

cat <<EOF > $TMP_DIR/resolve_depends.awk
    function get_ext(elem) { n = split(elem, b, "."); return b[n]; }
    function get_no_ext(elem) { return substr(elem,1, (length(elem) - length(get_ext(elem)) - 1)) }
    function ext_num(elem) { locext=get_ext(elem); return ((locext in ext) ? ext[locext] : "") }
    function num_to_ext(num) { for (x in ext) if (ext[x] == num) return x; }
    function fake_parent(child) { 
        CHILD_EXT_NUM = ext_num(child); 
        return ((CHILD_EXT_NUM != "") ? (get_no_ext(child) "." num_to_ext(CHILD_EXT_NUM - 1)) : ""); 
    }
    function get_parent(child,lvl)  { 
        if (child != "" && (lvl < 100)) {
            l_parent = (child in parnt) ? parnt [child] : fake_parent(child);
            return get_parent(l_parent, (lvl+1)) " " child;
        }
    } 
    BEGIN {
        $(echo "$PAR_BUILD_ORDER" | awk 'BEGIN {RS=" "} {print "ext [\"" $1 "\"] = " NR ";"}')
        $(grep -R "$PAR_DEPENDS_REGEXP" | sed -e "s|:${PAR_DEPENDS_REGEXP}| |" -e 's|"||' \
            | awk '{print "parnt [\"" $1 "\"] = \"" $2 "\";" }')
    }
    {print get_parent(\$1, 0)}
EOF

TMP_LST="$TMP_DIR/lst"

find * -type f | awk -f $TMP_DIR/resolve_depends.awk | (LC_ALL=C; sort) \
    | tee ${TMP_LST}.full.bare | awk '$1 != "skip" {print $NF}' > ${TMP_LST}.full

GIT_ROOT=$(git rev-parse --show-toplevel)
RELATIVE_PATH=${PWD#${GIT_ROOT}/}
git rev-parse HEAD > ${TMP_DIR}/$PAR_VERSION_FILE

git ls-files --others --full-name > ${TMP_LST}.not_yet_tracked

git diff --name-status $BASELINE_TAG \
    | awk '$1 != "D" {print $2}' > ${TMP_LST}.locally_changed

sed -e "s#^$RELATIVE_PATH/##" ${TMP_LST}.not_yet_tracked ${TMP_LST}.locally_changed > ${TMP_LST}.patch.lkp

grep --line-regexp -f ${TMP_LST}.patch.lkp ${TMP_LST}.full | cat > ${TMP_LST}.patch

cd "$OLDPWD"

for the_delta in full patch
do
    for the_target in $TARGETS
    do
        awk '!/^#/ && !/^ *$/ && !/PAR_.*=/ && $NF == "'$the_target'"' "$BUILDFILE" \
        | while read FILE_FILTER EXT_FLAG DISCARD
        do
            THE_FILE=${TMP_LST}.$the_delta
            
            if [[ "$EXT_FLAG" == "*" ]]
            then
                EXT_FILTER="."
                THE_FILE="${TMP_LST}.full"
            elif [[ "$EXT_FLAG" == "!" ]]
            then
                EXT_FILTER="."
                [[ "$the_delta" == "full" ]] && THE_FILE=/dev/null
            elif [[ "${the_delta}" == "patch" ]]
            then
                EXT_FILTER="\.(${PAR_STATELESS_EXTS// /|})$"
            else
                EXT_FILTER="."
            fi
            
            egrep "^${FILE_FILTER}" $THE_FILE | egrep "$EXT_FILTER" \
                | sed "s/^/$PAR_INC_PREF/"

        done > $TMP_DIR/${the_target}_${the_delta}${PAR_INSTALL_SCRIPT_EXT}
    done
done

cd "$ROOT_PATH"

zip -qr $ARCHIVE *

find $TMP_DIR -name "*_*${PAR_INSTALL_SCRIPT_EXT}" | zip -@ -qj "$ARCHIVE" "${TMP_DIR}/$PAR_VERSION_FILE"

[[ ${-/x} == $- ]]  && rm $TMP_DIR/* && rmdir $TMP_DIR

cd "$OLDPWD"

echo "$ARCHIVE"
