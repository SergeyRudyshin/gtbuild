#!/usr/bin/env bash

GTBUILD=${1:-"$PWD/gtbuild.sh"}

set -xue

mkdir -p "${TMP_DIR:=/tmp/$(date '+%F_%H%M%S').$$}"

cd $TMP_DIR

git init

cat <<EOF > build.gts
parameters.sql    *  main
comp_a            .  main
patches           !  main
PAR_BUILD_ORDER="tab idx ck vw trg sql"
PAR_STATELESS_EXTS="vw trg"
EOF

mkdir comp_a  patches
touch parameters.sql comp_a/a.tab comp_a/a.trg comp_a/a.idx comp_a/b.ck comp_a/b.tab comp_a/c.vw patches/cr_001.sql
echo "-- @depends on: comp_a/c.vw" > comp_a/b.tab
git add .
git commit -m "initial commit"
git tag -a -m "v1.0" "v1.0"

echo "-- @depends on: comp_a/b.ck" > comp_a/a.tab
echo "new" > comp_a/a.trg
echo "new" > comp_a/a.idx
echo "new" > patches/cr_002.sql

FN=$(bash -x $GTBUILD)

PTH=${FN%/*}
PID=${PTH#/*\.}

cat <<EOF > main_full.sql
@parameters.sql
@comp_a/c.vw
@comp_a/b.tab
@comp_a/b.ck
@comp_a/a.tab
@comp_a/a.idx
@comp_a/a.trg
EOF

cat <<EOF > main_patch.sql
@parameters.sql
@comp_a/a.trg
@patches/cr_002.sql
EOF

cat <<EOF > archive.lst.be
Name
----
build.gts
comp_a/
comp_a/a.trg
comp_a/b.tab
comp_a/c.vw
comp_a/b.ck
comp_a/a.idx
comp_a/a.tab
parameters.sql
patches/
patches/cr_002.sql
patches/cr_001.sql
main_patch.sql
main_full.sql
version
EOF

diff "$PTH/$PID/main_full.sql" main_full.sql

diff "$PTH/$PID/main_patch.sql" main_patch.sql

unzip -l $FN | awk '$4 !~ /^$/ {print $4}' > archive.lst.is

diff archive.lst.be archive.lst.is

echo SUCCESS
